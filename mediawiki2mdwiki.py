#!/usr/bin/env python
from __future__ import print_function, unicode_literals
import datetime
import logging
logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.INFO)
import os
import os.path
import argparse
import subprocess
from xml.etree import ElementTree as ET

DEF_NS = "http://www.mediawiki.org/xml/export-0.6/"

contributors = {}


def parse_contributor(cont_elem):
    logging.debug('cont_elem = %s', str(ET.tostring(cont_elem)))
    username_elem = cont_elem.find('{%s}username' % DEF_NS)
    if username_elem is not None:
        username = username_elem.text
        user_id = cont_elem.find('{%s}id' % DEF_NS).text
    else:
        ip_elem = cont_elem.find('{%s}ip' % DEF_NS)
        logging.debug('ip_elem = %s', str(ET.tostring(ip_elem)))
        if ip_elem is not None:
            username = ip_elem.text
            user_id = None
        else:
            raise ValueError("Who is the author?")

    if "@" not in username:
        username += " <unknown@example.com>"

    if (user_id is not None) and (user_id not in contributors):
        contributors[user_id] = username

    logging.debug('user_id = %s', user_id)
    logging.debug('username = %s', username)
    return user_id, username


def parse_revs(rev_elem):
    logging.debug('rev_elem = %s', str(ET.tostring(rev_elem)))
    out = {
        'id': rev_elem.find('{%s}id' % DEF_NS).text,
        'text': rev_elem.find('{%s}text' % DEF_NS).text,
    }

    comment_elem = rev_elem.find('{%s}comment' % DEF_NS)
    if comment_elem:
        out['comment'] = comment_elem.text
    timestamp = rev_elem.find('{%s}timestamp' % DEF_NS).text

    # <timestamp>2006-05-07T01:36:30Z</timestamp>
    out['timestamp'] = datetime.datetime.strptime(timestamp,
                                                  '%Y-%m-%dT%H:%M:%SZ')
    out['contributor'] = parse_contributor(
        rev_elem.find('{%s}contributor' % DEF_NS))

    logging.debug('out = %s', out)
    return out


def parse_page(pg_elem):
    logging.debug('pg_elem = %s', pg_elem)
    page_dict = {
        'title': pg_elem.find('{%s}title' % DEF_NS).text,
        'revs': []
    }

    for rev in pg_elem.findall('{%s}revision' % DEF_NS):
        rev = parse_revs(rev)
        page_dict['revs'].append(rev)

    logging.debug('page_dict = %s', page_dict)
    return page_dict


def parse_XML(fname):
    out = []
    logging.debug('fname = %s', fname)
    logging.debug('curdir = %s', os.curdir)
    logging.debug('files = %s', os.listdir('.'))

    xml_parser = ET.parse(fname)
    xml_parser.getroot()
    for page in xml_parser.findall('.//{%s}page' % DEF_NS):
        out.append(parse_page(page))

    return out


def write_rev(rev_tuple):
    title = rev_tuple[0].strip().replace(' ', '').replace('/', '')
    revision = rev_tuple[1]
    logging.debug('revision:\n%s', revision)
    contr = revision['contributor']
    if len(contr) == 2:
        contributor = contr[1]
    else:
        contributor = contr[0]
    logging.debug('contributor = %s', contributor)

    # We don't want to store just references to images
    if title[-4:].lower() in ('.png', '.jpg'):
        return None

    comm_msg = "rev: " + revision['id']
    if 'comment' in revision:
        comm_msg += " %s" % revision['comment']

    date_str = revision['timestamp'].strftime('%Y-%m-%dT%H:%M:%S')

    with open('%s.wiki' % title, 'w') as wiki_file:
        print(revision['text'].encode('utf8'), file=wiki_file)

    subprocess.check_call(['git', 'add', '%s.wiki' % title])
    try:
        subprocess.check_call(
            ('git commit -m "%s" --author="%s" --date="%s" %s.wiki') % 
            (comm_msg, contributor, date_str, title), shell=True)
    except subprocess.CalledProcessError as ex:
        logging.debug('ex = %s', ex)
        if ex.returncode != 1:
            raise


def fill_wiki_repo(pgs, wdir):
    os.chdir(os.path.realpath(wdir))
    if not os.path.exists('.git'):
        subprocess.check_call(['git', 'init'])

    revs = {}
    for pg in pgs:
        for rev in pg['revs']:
            revs[rev['id']] = pg['title'], rev

    for rev in sorted(revs.keys()):
        write_rev(revs[rev])


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('xmlfilename')
    parser.add_argument('wikidir')
    opts = parser.parse_args()
    logging.debug('opts = %s', opts)

    pages = parse_XML(opts.xmlfilename)
    fill_wiki_repo(pages, opts.wikidir)


if __name__ == '__main__':
    main()
