package mediawiki2mdwiki;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Wikipedia {
	private ArrayList<Page> pages = new ArrayList<Page>();

	public Wikipedia(Element doc) {
		NodeList pg_elems = doc
				.getElementsByTagNameNS("page", Constants.DEF_NS);
		Node pg = null;
		for (int pg_i = 0; pg_i < pg_elems.getLength(); pg_i++) {
			pg = pg_elems.item(pg_i);
			pages.add(new Page((Element) pg));
		}
	}

	public final void generate(String wikiDir) {
		System.setProperty("user.dir", new File(wikiDir).getAbsoluteFile()
				.getAbsolutePath());

		if (!(new File(".git").getAbsoluteFile()).isDirectory()) {
			try {
				Runtime.getRuntime().exec("git init").waitFor();
			} catch (RuntimeException | InterruptedException | IOException e) {
				System.out.println(e.getMessage());
			}
		}

		ArrayList<Revision> revs = new ArrayList<Revision>();

		for (Page pg : pages) {
			for (Revision rev : pg.revs) {
				revs.add(rev);
			}
		}

		for (Revision rev : revs) {
			rev.generate();
		}
	}
}