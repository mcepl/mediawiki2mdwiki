package mediawiki2mdwiki;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Page {
	public String title;
	public ArrayList<Revision> revs = new ArrayList<Revision>();

	public Page(Element pg_elem) {
		title = pg_elem.getElementsByTagNameNS(Constants.DEF_NS, "title")
				.item(0).getTextContent();
		NodeList revisions = pg_elem.getElementsByTagNameNS(Constants.DEF_NS,
				"revision");
		for (int i = 0; i < revisions.getLength(); i++) {
			revs.add(new Revision((Element) revisions.item(i), this));
		}
	}
}