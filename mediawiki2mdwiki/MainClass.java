package mediawiki2mdwiki;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class MainClass {
	public static void main(String[] args) {

		if (args.length == 2) {
			System.out.println("XML file = " + args[0]);
			System.out.println("Wiki git repository = " + args[1]);

			DocumentBuilderFactory builderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = null;
			try {
				builder = builderFactory.newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			}

			Document xmldoc = null;
			try {
				xmldoc = builder.parse(args[0]);
			} catch (SAXException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (xmldoc != null) {
				Wikipedia wp = new Wikipedia(xmldoc.getDocumentElement());
				wp.generate(args[1]);
			}
		} else {
			throw new RuntimeException(
					"Need two arguments: XML file name and Wiki Git repository dirname");
		}
	}
}