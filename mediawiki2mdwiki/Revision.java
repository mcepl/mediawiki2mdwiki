package mediawiki2mdwiki;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.w3c.dom.Element;

public class Revision {
	public String id;
	public String comment;
	public String text;
	public Date timestamp = null;
	public Contributor contributor;
	public Page parent;

	public Revision(Element rev, Page par) {
		String time_str = rev
				.getElementsByTagNameNS(Constants.DEF_NS, "timestamp").item(0)
				.getTextContent();
		// <timestamp>2006-05-07T01:36:30Z</timestamp>
		// '%Y-%m-%dT%H:%M:%SZ'
		id = rev.getElementsByTagNameNS(Constants.DEF_NS, "id").item(0)
				.getTextContent();
		comment = rev.getElementsByTagNameNS(Constants.DEF_NS, "comment")
				.item(0).getTextContent();
		text = rev.getElementsByTagNameNS(Constants.DEF_NS, "text").item(0)
				.getTextContent();
		contributor = new Contributor((Element) rev.getElementsByTagNameNS(
				Constants.DEF_NS, "contributor").item(0));
		parent = par;

		try {
			timestamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
					.parse(time_str);
		} catch (ParseException e) {
			timestamp = new Date(0);
		}
	}

	public final void generate() {
		String date_str = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS")
				.format(timestamp);
		String title = parent.title;
		String commit_msg = ""; // FIXME

		try (BufferedWriter w = new BufferedWriter(new FileWriter(new File(title
				+ ".wiki").getAbsoluteFile()))) {
			w.write(text + "\n");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Runtime runtime = Runtime.getRuntime();
		try {
			runtime.exec("git add " + title + ".wiki").waitFor();
			runtime.exec(
					"git commit " + title + ".wiki" + " -m '" + commit_msg
							+ "' --author='" + contributor.username
							+ "' --date='" + date_str + "'").waitFor();
		} catch (InterruptedException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}