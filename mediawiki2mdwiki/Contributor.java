package mediawiki2mdwiki;

import org.w3c.dom.Element;

public class Contributor {
	public int id;
	public String username;

	public Contributor(Element cont) {
		id = Short.parseShort(cont
				.getElementsByTagNameNS(Constants.DEF_NS, "id").item(0)
				.getTextContent());
		username = cont.getElementsByTagNameNS(Constants.DEF_NS, "username")
				.item(0).getTextContent();
	}
}